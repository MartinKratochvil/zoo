package cz.nitram.zoo;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Scanner;

public class Operation {

    private final HashMap<String, String> operations = new HashMap<>()
    {{
        put("+", "add");
        put("-", "remove");
        put("print", "show");
        put("exit", "end");
    }};

    private final HashMap<String, String> animals = new HashMap<>()
    {{
        put("kočka", "cat");
        put("pes", "dog");
        put("slon", "elephant");
    }};


    private final Scanner scanner= new Scanner(System.in);


    public void handle(String method) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        String methodName = this.operations.get(method.toLowerCase());
        Operation.class.getMethod(methodName).invoke(this);
    }
    
    
    public void add() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        System.out.println("Zadej typ zvířete:");
        String type = scanner.next();
        String methodName = this.animals.get(type.toLowerCase());

        Animal animal = (Animal) AddAnimal.class.getMethod(methodName).invoke(this);
        Zoo.addAnimal(animal);

        System.out.println("Zvíře úspěšně přidáno!");
    }


    public void remove()
    {
        boolean removed = Zoo.removeAnimal(getName());
        System.out.println("Zvíře " + (removed ? "úspěšně odstraněno!" : "neexistuje!"));
    }


    public void show() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, IllegalArgumentException {
        Animal animal = Zoo.getAnimal(getName());

        if (animal == null) {
            System.out.println("Zvíře neexistuje!");

            return;
        }

        String methodName = animal.getClass().getSimpleName().toLowerCase();

        System.out.println("Jméno: " + animal.getName());
        System.out.println("Věk: " + animal.getAge());
        System.out.println("Váha: " + animal.getWeight());

        ShowAnimal.class.getMethod(methodName, Animal.class).invoke(this, animal);
    }


    public void end()
    {
        System.exit(0);
    }


    private String getName()
    {
        System.out.println("Zadej jméno zvířete:");
        return scanner.next().toLowerCase();
    }
}
