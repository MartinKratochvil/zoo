package cz.nitram.zoo;

public abstract class Animal
{

    private String name;

    private short weight;

    private byte age;


    public Animal(String name, short weight, byte age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
    }


    public String getName() {
        return name;
    }


    public short getWeight() {
        return weight;
    }


    public byte getAge() {
        return age;
    }
}
