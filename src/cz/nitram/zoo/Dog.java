package cz.nitram.zoo;

public class Dog extends Animal implements IMakeSound
{

    private String huntingSound = "Vrrrrrr Haf Haf";

    private String chillingSound = "Haf!";

    private boolean isHunt;


    public Dog(String name, short weight, byte age, boolean isHunt)
    {
        super(name, weight, age);
        this.isHunt = isHunt;
    }


    @Override
    public String makeSound()
    {
        return isHunt ? huntingSound : chillingSound;
    }


    public boolean isHunt()
    {
        return isHunt;
    }
}
