package cz.nitram.zoo;

import java.util.Scanner;

public class AddAnimal
{

    private static Scanner scanner = new Scanner(System.in);

    private static String name;

    private static Byte age;

    private static Short weight;


    private static void inputAnimal()
    {
        System.out.println("Zadej jméno:");
        name = scanner.next();

        System.out.println("Zadej věk:");
        age = scanner.nextByte();

        System.out.println("Zadej váhu:");
        weight = scanner.nextShort();
    }


    public static Animal cat()
    {
        inputAnimal();

        System.out.println("Spí kočka v tuto dobu?");
        boolean isSleep = scanner.nextBoolean();

        return new Cat(name, weight, age, isSleep);
    }


    public static Animal dog()
    {
        inputAnimal();

        System.out.println("Je pes v lovecké náladě?");
        boolean isHunt = scanner.nextBoolean();

        return new Dog(name, weight, age, isHunt);
    }


    public static Animal elephant()
    {
        inputAnimal();

        return new Elephant(name, weight, age);
    }
}
