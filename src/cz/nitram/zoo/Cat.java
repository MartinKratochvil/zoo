package cz.nitram.zoo;

public class Cat extends Animal implements IMakeSound
{

    private String sleepingSound = "Chrrrr meow meow meow";

    private String awakeSound = "Meeooow";

    private boolean isSleep;


    public Cat(String name, short weight, byte age, boolean isSleep)
    {
        super(name, weight, age);
        this.isSleep = isSleep;
    }


    @Override
    public String makeSound()
    {
        return isSleep ? sleepingSound : awakeSound;
    }


    public boolean isSleep()
    {
        return isSleep;
    }
}
