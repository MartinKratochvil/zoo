package cz.nitram.zoo;

public class Elephant extends Animal implements IMakeSound
{

    private String sound = "Frrrrrrrrrr";


    public Elephant(String name, short weight, byte age)
    {
        super(name, weight, age);
    }


    @Override
    public String makeSound()
    {
        return sound;
    }
}
