package cz.nitram.zoo;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;


public class Zoo
{

    private static HashMap<String, Animal> animals = new HashMap<>();
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Scanner scanner = new Scanner(System.in);
        Operation action = new Operation();

        while(true) {
            try
            {
                System.out.println("Zadej operaci (+, print, -, exit):");
                action.handle(scanner.next());
            }
            catch (Exception e)
            {
                System.out.println("Zadaj jsi špatný vstup!");
            }
        }
    }


    public static void addAnimal(Animal animal)
    {
        animals.put(animal.getName().toLowerCase(), animal);
    }


    public static boolean removeAnimal(String name)
    {
        if (animals.containsKey(name))
        {
            animals.remove(name.toLowerCase());

            return true;
        }

        return false;
    }


    public static Animal getAnimal(String name)
    {
        return animals.get(name.toLowerCase());
    }
}
