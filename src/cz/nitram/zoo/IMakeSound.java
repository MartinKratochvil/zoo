package cz.nitram.zoo;

public interface IMakeSound
{
    String makeSound();
}
