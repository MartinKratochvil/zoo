package cz.nitram.zoo;

public class ShowAnimal
{

    public static void cat(Animal animal)
    {
        Cat cat = (Cat) animal;
        System.out.println("Spí: " + cat.isSleep());
        System.out.println("Zvuk: " + cat.makeSound());
    }


    public static void dog(Animal animal)
    {
        Dog dog = (Dog) animal;
        System.out.println("Je v lovecké náladě: " + dog.isHunt());
        System.out.println("Zvuk: " + dog.makeSound());
    }


    public static void elephant(Animal animal)
    {
        Elephant elephant = (Elephant) animal;
        System.out.println("Zvuk: " + elephant.makeSound());
    }
}
